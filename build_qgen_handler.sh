rm model_store/qgen_serve.mar
torch-model-archiver --model-name "qgen_serve" --version 1.0 --serialized-file ./qgen_pretrained/pytorch_model.bin --handler "./qgen_handler.py" --extra-files "./qgen_pretrained/config.json,./qgen_pretrained/spiece.model,./qgen_pretrained/tokenizer_config.json,./qgen_pretrained/special_tokens_map.json"
mv qgen_serve.mar model_store