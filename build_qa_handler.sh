rm model_store/qa_serve.mar
torch-model-archiver --model-name "qa_serve" --version 1.0 --serialized-file ./qa_pretrained/pytorch_model.bin --handler "./qa_handler.py" --extra-files "./qa_pretrained/config.json,./qa_pretrained/spiece.model,./qa_pretrained/tokenizer_config.json,./qa_pretrained/special_tokens_map.json"
mv qa_serve.mar model_store