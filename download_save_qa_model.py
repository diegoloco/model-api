from pathlib import Path
import os
import json
import torch
from transformers import (AutoModelForSequenceClassification, AutoTokenizer, AutoModelForQuestionAnswering,
 AutoModelForTokenClassification, AutoConfig)
from transformers import set_seed

model_cache = "/run/media/pablo/084A2BF94A2BE264/ml/model_cache"
NEW_DIR = "./qa_pretrained"

set_seed(1)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

tokenizer = AutoTokenizer.from_pretrained("elgeish/cs224n-squad2.0-albert-base-v2", cache_dir=model_cache)

model = AutoModelForQuestionAnswering.from_pretrained("elgeish/cs224n-squad2.0-albert-base-v2", cache_dir=model_cache)

model.save_pretrained(NEW_DIR)
tokenizer.save_pretrained(NEW_DIR)