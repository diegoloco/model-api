from pathlib import Path
import os
import json
import torch
from transformers import (T5ForConditionalGeneration, AutoModelForSequenceClassification, AutoTokenizer, AutoModelForQuestionAnswering,
                          AutoModelForTokenClassification, AutoConfig)
from transformers import set_seed

model_cache = "/run/media/pablo/084A2BF94A2BE264/ml/model_cache"

NEW_DIR = "./qgen_pretrained"
MODEL_NAME = "ramsrigouthamg/t5_boolean_questions"

set_seed(1)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

tokenizer = AutoTokenizer.from_pretrained("t5-base", cache_dir=model_cache)

model = T5ForConditionalGeneration.from_pretrained(MODEL_NAME, cache_dir=model_cache)

model.save_pretrained(NEW_DIR)
tokenizer.save_pretrained(NEW_DIR)
