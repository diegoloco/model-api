from abc import ABC
import json
import logging
import os
import ast

import torch
from transformers import T5Tokenizer, T5ForConditionalGeneration

from ts.torch_handler.base_handler import BaseHandler

logger = logging.getLogger(__name__)


class TransformersQuestionGenerationHandler(BaseHandler, ABC):
    """
    Transformers text classifier handler class. This handler takes a text (string) and
    as input and returns the classification text based on the serialized transformers checkpoint.

    Adapted from https://medium.com/analytics-vidhya/deploy-huggingface-s-bert-to-production-with-pytorch-serve-27b068026d18
    """
    def __init__(self):
        super(TransformersQuestionGenerationHandler, self).__init__()
        self.initialized = False

    def greedy_decoding (self, inp_ids,attn_mask):
        greedy_output = self.model.generate(input_ids=inp_ids, attention_mask=attn_mask, max_length=256)
        Question =  self.tokenizer.decode(greedy_output[0], skip_special_tokens=True,clean_up_tokenization_spaces=True)
        return Question.strip().capitalize()


    def beam_search_decoding (self, inp_ids,attn_mask):
        beam_output = self.model.generate(input_ids=inp_ids,
                                        attention_mask=attn_mask,
                                        max_length=256,
                                    num_beams=10,
                                    num_return_sequences=10,
                                    no_repeat_ngram_size=2,
                                    early_stopping=True
                                    )
        questions = [self.tokenizer.decode(out, skip_special_tokens=True, clean_up_tokenization_spaces=True) for out in
                    beam_output]
        return [question.strip().capitalize() for question in questions]


    def topkp_decoding (self, inp_ids,attn_mask):
        topkp_output = self.model.generate(input_ids=inp_ids,
                                        attention_mask=attn_mask,
                                        max_length=256,
                                    do_sample=True,
                                    top_k=40,
                                    top_p=0.80,
                                    num_return_sequences=10,
                                        no_repeat_ngram_size=2,
                                        early_stopping=True
                                    )
        questions = [self.tokenizer.decode(out, skip_special_tokens=True,clean_up_tokenization_spaces=True) for out in topkp_output]
        return [question.strip().capitalize() for question in questions]

    def initialize(self, ctx):
        self.manifest = ctx.manifest

        properties = ctx.system_properties
        model_dir = properties.get("model_dir")
        self.device = torch.device("cuda:" + str(properties.get("gpu_id")) if torch.cuda.is_available() else "cpu")

        # Read model serialize/pt file
        self.model = T5ForConditionalGeneration.from_pretrained(model_dir)
        self.tokenizer = T5Tokenizer.from_pretrained("t5-base")

        self.model.to(self.device)
        self.model.eval()
        

        logger.debug('Transformer model from path {0} loaded successfully'.format(model_dir))

        self.initialized = True

    def preprocess(self, data):
        """ Very basic preprocessing code - only tokenizes. 
            Extend with your own preprocessing steps as needed.
        """
        text = data[0].get("data")
        if text is None:
            text = data[0].get("body")
            
        context = text["context"]

        inputs = self.tokenizer.encode_plus(
            context,
            return_tensors="pt"
        )
        return inputs

    def inference(self, inputs):
        """
        Generate questions for a text using a trained transformer model.
        """
        input_ids, attention_masks = inputs["input_ids"].to(self.device), inputs["attention_mask"].to(self.device)

        output = self.beam_search_decoding(input_ids,attention_masks)

        return [output]

    def postprocess(self, inference_output):
        return json.dumps({"output": inference_output[0]})


_service = TransformersQuestionGenerationHandler()

def handle(data, context):
    try:
        if not _service.initialized:
            _service.initialize(context)

        if data is None:
            return None

        data = _service.preprocess(data)
        data = _service.inference(data)
        data = _service.postprocess(data)

        return [data]
    except Exception as e:
        raise e